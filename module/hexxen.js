/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", () => {
  console.log(`${Hexxen.logPrefix}Initializing system`);

  CONFIG.Hexxen = CONFIG.Hexxen || {};
  const urls = {
    'git-wiki': 'https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/wikis',
    'git-releases': 'https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/releases',
    'git-issues': 'https://gitlab.com/mbrunninger/fvtt-hexxen-1733-core/-/issues'
  }
  CONFIG.Hexxen.urls = urls;

  // Register Handlebars helper for use in HTML templates
  HexxenHandlebarsHelper.registerHelpers();

  // Style element for dynamic CSS style modifications
  $('head').append('<style id="hexxen-styles"></style>');

  // Inject system logo and register listener to show an About dialog
  HexxenLogo.inject();
  HexxenIssueTracker.inject();

	// Define custom classes
  CONFIG.Actor.documentClass = HexxenActor;
  CONFIG.Item.documentClass = HexxenItem;
  // CONFIG.Item.dataModels.skill = SkillData;

  // Manipulate DiceTerm.matchTerm() to allow for two-character denominations starting with 'h'
  // Wichtig: Muss bereits in init erfolgen! Sonst können Probleme beim Rekonstruieren der ChatMeldungen auftreten!
  HexxenRollHelper.injectTwoCharacterDiceTermPatch();
  // Assemble template paths
  HexxenRoll.HX_CHAT_TEMPLATE = `${Hexxen.basepath}/templates/dice/roll.html`;
  HexxenRoll.HX_TOOLTIP_TEMPLATE = `${Hexxen.basepath}/templates/dice/tooltip.html`;
  HexxenRoll.HX_ROLL_TOTAL_TEMPLATE = `${Hexxen.basepath}/templates/dice/roll-total.html`;
  CONFIG.Hexxen.ROLL_RESULT_SYMBOLS = { // FIXME: i18n
    '+': {path:`${Hexxen.basepath}/img/dice/symerfolg.png`, label: 'Erfolg'},
    '-': {path:`${Hexxen.basepath}/img/dice/symmisserfolg.png`, label: 'Misserfolg'}, // FIXME: nur für /r??
    '*': {path:`${Hexxen.basepath}/img/dice/symesprit.png`, label: 'Espritstern'},
    'b': {path:`${Hexxen.basepath}/img/dice/symblut.png`, label: 'Bluttropfen'},
    'e': {path:`${Hexxen.basepath}/img/dice/symelixier.png`, label: 'Elixir'},
    'f': {path:`${Hexxen.basepath}/img/dice/symfluch.png`, label: 'Fluch'}
  };
  CONFIG.Hexxen.BONUS_CAP = 5;

  // Register rolls, dice and coins
  CONFIG.Dice.rolls.unshift(HexxenRoll); // new default Roll class
  CONFIG.Dice.rolls.push(SDRRoll);
  [
    HexxenDie, GamemasterDie, JanusBonusDie, JanusMalusDie, SegnungsDie, BlutDie, ElixierDie, FluchDie,
    ImminenceCoin, CoupCoin, IdeaCoin, BlessingCoin, RageCoin, AmbitionCoin
  ].forEach(t => {
    CONFIG.Dice.terms[t.DENOMINATION] = t;
  });

  // Registering translation keys for Actors and Items
  Object.assign(CONFIG.Actor.typeLabels, {
    'character': 'HEXXEN.ACTORTYPE.character',
    'character2e': 'HEXXEN.ACTORTYPE.character2e',
    // 'npc': 'HEXXEN.ACTORTYPE.npc',
    'npc-leader': 'HEXXEN.ACTORTYPE.npc-leader',
    'npc-bande': 'HEXXEN.ACTORTYPE.npc-mob'
  });
  Object.assign(CONFIG.Item.typeLabels, {
    'item': 'HEXXEN.ITEMTYPE.item',
    'skill': 'HEXXEN.ITEMTYPE.skill',
    'state': 'HEXXEN.ITEMTYPE.state',
    'motivation': 'HEXXEN.ITEMTYPE.motivation',
    'npc-power': 'HEXXEN.ITEMTYPE.npc-power',
    'power': 'HEXXEN.ITEMTYPE.power',
    'profession': 'HEXXEN.ITEMTYPE.profession',
    'state': 'HEXXEN.ITEMTYPE.state',
    'regulation': 'HEXXEN.ITEMTYPE.regulation',
    'role': 'HEXXEN.ITEMTYPE.role'
  });

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("hexxen", JaegerSheet, {
    label: game.i18n.localize('HEXXEN.PC-Sheet.Title'),
    types: ["character", "character2e"],
    makeDefault: true
  });
  // Actors.registerSheet("hexxen", NpcSheet, {
  //   label: game.i18n.localize('HEXXEN.NPC-Sheet.Title'),
  //   types: ["npc"],
  //   makeDefault: true
  // });
  Actors.registerSheet("hexxen", NpcLeaderSheet, {
    label: game.i18n.localize('HEXXEN.NPC-Sheet.TitleLeaderDeprecated'),
    types: ["npc-leader"],
    makeDefault: true
  });
  Actors.registerSheet("hexxen", NpcBandeSheet, {
    label: game.i18n.localize('HEXXEN.NPC-Sheet.TitleMobDeprecated'),
    types: ["npc-bande"],
    makeDefault: true
  });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("simple", SimpleItemSheet, {
    label: game.i18n.localize('HEXXEN.Item-Sheet.Title'),
    types: ["item"],
    makeDefault: true
  });
  Items.registerSheet("hexxen", RuleItemSheet, {
    label: game.i18n.localize('HEXXEN.RuleItem-Sheet.Title'),
    types: ["skill", "state", "role", "profession", "motivation", "power", "state", "regulation", "npc-power"],
    makeDefault: true
  });

  // Configure initiative for CombatTracker
	CONFIG.Combat.initiative = {
	  formula: "@ini.value",
    decimals: 1 // TODO: 0, sobald SC INI Vorrang im CombatTracker
  };

  // FIXME: preload templates??

  // FIXME: preload some images
  // Hexxen.preload(
  //   `${Hexxen.basepath}/img/parchment_red.png`,
  //   `${Hexxen.basepath}/img/Rabenkasten_oben_small_braun.png`
  //   `${Hexxen.basepath}/img/Rabenkasten_weit_unten_small_braun.png`
  // );

  // Register callbacks for macro creation
  Hooks.on('hotbarDrop', HexxenRollHelper.createMacro);

  // Inject application alignment code into FVTT event listener (entity-link)
  HexxenAppAlignmentHelper.install();
  
  // Inject custom roll command
  HexxenSpecialCommandHelper.install();
  
  Hexxen.registerSettings();

  console.log(`${Hexxen.logPrefix}Initialization done`);
});



Hooks.on('chatMessage', (x,y,z) => { return HexxenRollHelper.processChatCommand(x,y,z); }); // FIXME: Problem mit Initialisierungsreihenfolge mit SDR
Hooks.on('preCreateChatMessage', (data, options, userId) => { return HexxenRollHelper.pimpChatMessage(data, options, userId); }); // TODO: CR in Foundry

Hooks.once("ready", async function() {
  console.log(`${Hexxen.logPrefix}Ready Hook called`);

  HexxenRollHelper.checkSystemReqirements();

  console.log(`${Hexxen.logPrefix}Ready Hook done`);
});



class Hexxen {
  static get scope() {
    return game.system.id;
  }

  static get basepath() {
    // TODO: evtl. so machen: https://stackoverflow.com/questions/2255689/how-to-get-the-file-path-of-the-currently-executing-javascript-code/2255727
    return `/systems/${Hexxen.scope}`;
  }

  static get title() {
    return game.system.title;
  }

  static get logPrefix() {
    return `${this.title} | `;
  }

  static preload() {
    for (let i = 0; i < arguments.length; i++) {
      const img = arguments[i];
      console.log(`${Hexxen.logPrefix}Preloading ${img}`)
      const image = new Image();
      image.src = img;
    };
  }

  static get COLOR_THEME_SETTING_KEY() { return 'colorTheme' }

  static registerSettings() {

    game.settings.register(Hexxen.scope, this.COLOR_THEME_SETTING_KEY, {
      name: 'Farbschema',
      hint: 'Wähle ein Farbschema für den Charakterbogen. (Reload erforderlich!)',
      scope: 'client',
      config: true,
      type: String,
      choices: { 'theme-green': 'Grün', 'theme-red': 'Rot' },
      default: 'theme-green',
      onChange: () => { location.reload(); }
    });

    HexxenRollSettings.registerSettings();
    HexxenAppAlignmentHelper.registerSettings();
  }
}



class HexxenLogo {

  static inject() {
    // use jQuery to inject the logo into the DOM
    $(`<img id="hexxen-logo" src="${Hexxen.basepath}/img/hexxenlogo.png" height="65px" />`)
      .insertAfter('img#logo');
    $('img#hexxen-logo')
      .on('click', (event) => { 
        new HexxenAbout().render(true); 
      });
  }
}



class HexxenIssueTracker {

  static inject() {
    Hooks.on('renderSettings', (app, html, data) => {
      $(`<button data-action="hexxen-issue"><i class="fas fa-bug"></i>Fehler melden (Hexxen 1733)</button>`)
        .insertAfter('#settings button[data-action=wiki]')
        .on('click', (event) => { 
          event.preventDefault();
          event.stopImmediatePropagation();
          window.open(CONFIG.Hexxen.urls['git-issues']);
        });
    });
  }
}



class HexxenAbout extends Application {

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['hexxen', 'about'],
      id: 'hexxen-about',
      title: `About Game System ${Hexxen.title}`,
      template: `${Hexxen.basepath}/templates/about.html`,
      width: 600,
      height: 450
    });
  }

  getData() {
    return  { 
      'hx-basepath': Hexxen.basepath,
      'hx-git-wiki-basepath': CONFIG.Hexxen.urls['git-wiki'],
      'hx-git-releases': CONFIG.Hexxen.urls['git-releases'],
      'hx-git-issues': CONFIG.Hexxen.urls['git-issues']
    };
  }
}
