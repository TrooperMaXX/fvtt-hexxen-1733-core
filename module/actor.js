﻿/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

// class SkillData extends foundry.abstract.DataModel {
//   static defineSchema() {
//     return {};
//   }
//   //   const fields = foundry.data.fields;
//   //   return {
//   //     "attribute": new fields.StringField(),
//   //     "value": new fields.NumberField({
//   //       required: true,
//   //       initial: 1,
//   //       integer: true
//   //     })
//   //   };
//   // }

//   static migrateData(source) {
//     return super.migrateData(source);
//   }
// }

/* Class hierarchy:
 * HexxenActor
 *   + Actor
 *     + ClientDocumentMixin(BaseActor)
 *       + BaseActor
 *         + Document
 *           + DataModel
 */
class HexxenActor extends Actor {

  static renamedSkill(skill) {
    const mapping = {
      // 1E --> 2E
      'Erkennen': 'Recherche',
      'Land und Leute': 'Praktisches Wissen',
      'Wissensgebiete': 'Akademisches Wissen',
      'Mystik': 'Mystisches Wissen',
      'Hexenkunst': 'Mystisches Wissen',
      'Reflexe': 'Ausweichen',
      'Reiten': 'Akrobatik',
      'Schiffsnavigation': 'Seefahrt',
      // 2E --> 1E
      'Recherche': 'Erkennen',
      'Praktisches Wissen': 'Land und Leute',
      'Akademisches Wissen': 'Wissensgebiete',
      'Mystisches Wissen': 'Mystik',
      'Seefahrt': 'Schiffsnavigation'
      // TODO uneindeutige Änderungen??
    }
    return mapping[skill] ?? skill;
  } 

  static async create(data, options={}) {
    // inject custom flag
    data = HexxenEntityHelper.addCustomFlag(data); // FIXME passt das mit dem custom flag noch? Wann wird create aufgerufen? Ist core.sourceID besser geeignet?
    
    //make Jäger default Friendly and Linked on Creation
      data.prototypeToken = data.prototypeToken || {};
      
      let defaults = {};
      if (data.type === "character" || data.type === "character2e") {
          defaults = {
            actorLink: true,
            disposition: CONST.TOKEN_DISPOSITIONS.FRIENDLY,
          };
      }

      mergeObject(data.prototypeToken, defaults, { overwrite: false });
    
    const actor = await super.create(data, options);

    if (
      actor.isHunter2e // only for 2e actors, but NOT
      && !(data instanceof HexxenActor) // e.g. when duplicating an actor in the world
      && !(options.fromCompendium ?? false) // when adding an actor from a compendium to the world
      && !(options.pack) // when adding an actor to a compendium
    ) {
      // TODO besser ein skillsInitialized flag setzen
      await this.addHunter2eSkillItems(actor);
    }
    
    return actor;
  }

  static async addHunter2eSkillItems(actor) {
    const pack = game.packs.get("hexxen-1733.hexxen-skills-2e");
    if (pack.documentName !== 'Item') return;

    const items = [];
    for (let item of pack.index) {
      if (item.type !== 'skill') continue;

      items.push(await fromUuid(item.uuid));
    }
    await actor.createEmbeddedDocuments('Item', items);
  }

  constructor(...args) {
    // Foundry 11:
    // The actor is either created with it's saved data or with the associated template data.

    // in [DataModel]
    // - this._initializeSource(...) --> calls this.constructor.migrateData(...)
    // - this._configure(options)
    // - this.validate(...)
    // - this._initialize(options)
    //   in [ClientDocumentMixin]
    //   - super._initialize(options)
    //   - this._safePrepareData() # nur falls game._documentsReady
    super(...args);
  }

  /** @inheritdoc */
  _initializeSource(source, options={}) {
    // if (source.type === 'character') {
    //   for (let skill of Object.values(source.system.skills)) {
    //     if (!skill.label) continue;
    //     source.items.push({
    //       name: skill.label,
    //       type: 'skill',
    //       system: {
    //         type: 'general',
    //         attribute: skill.attribute,
    //         value: skill.value,
    //         migrate: true
    //       }
    //     });
    //   }
    //   delete source.system.skills;
    // }

    return super._initializeSource(source, options);
  }

  /**
   * 
   * @param {*} data full actor data
   */
  static migrateData(data) {
    return super.migrateData(data);
  }

  get isHunter() {
    return ["character", "character2e"].includes(this.type);
  }

  get isHunter1e() {
    return "character" === this.type;
  }

  get isHunter2e() {
    return "character2e" === this.type;
  }

  get isNpc() {
    return !this.isHunter;
  }

  get level() {
    return this.system.level?.value || 0; // TODO: Fallback, besser in update sicherstellen
  }

  // /** @override */
  // // FIXMENOW: _initialze seit 0.8.x, sobald _migrate überprüft
  // initialize() {
  //   // TODO: Check if actor data have to be migrated, otherwise immediately call super.initialize()
  //   // FIXME: issue FVTT#3107, FVTT#3407
  //   HexxenUpdateQueue.enqueue(async () => {
  //     await this._migrateData();
  //     // TODO: vorgelagerte Prüfung (z.B. _needToMigrate())

  //     super._initialize();
  //   });
  // }

  _initialize() {
    // at this point "id" is only available from "_source._id"
    super._initialize();
  }

  /**
   * @override
   * Prepare data for the Entity whenever the instance is first created or later updated.
   * This method can be used to derive any internal attributes which are computed in a formulaic manner.
   */
  prepareData() {
    // in [ClientDocumentMixin]
    // - this.system.prepareBaseData();
    // - this.prepareBaseData();
    // - this.prepareEmbeddedDocuments();
    // - this.system.prepareDerivedData();
    // - this.prepareDerivedData();
    super.prepareData();
  }

  prepareBaseData() {
    const actorData = this.system;

    // Abgeleitete Basisdaten für Jaeger berechnen
    if (this.isHunter) {
      // TODO: evtl. But in Foundry: Import entfernt die alten Actor-Daten nicht.
      // TODO: evtl. Bug in Foundry bzgl. template.json und entfernte Attribute
      // deshalb hier entfernen um Datenstruktur sauber zu halten
      delete actorData["biography"];
      delete actorData["states-text"];
      delete actorData["skills-text"];
      delete actorData["powers-text"];
      delete actorData["combat-text"];
      delete actorData["items-text"];

      // Max-Werte für Basis- und Puffer-LEP berechnen
      // TODO: automatisieren (rules)
      actorData.health.min = -10;
      actorData.health.max = 7 + actorData.attributes.KKR.value
                                + actorData.attributes.WIL.value
                                + actorData.temp["lep-bonus"];
      actorData.power.min = 0;
      actorData.power.max = 10;

      // INI, PW und AP berechnen
      // TODO: automatisieren (rules)
      actorData.ini.value = actorData.attributes.SIN.value
                                + actorData.attributes.GES.value
                                + actorData.temp["ini-bonus"];
      actorData.calc = actorData.calc || {};
      actorData.calc.ap = 6 - actorData.temp.pw
                                + actorData.temp["ap-bonus"];

      if (this.isHunter1e) {
        actorData.health.max += actorData.skills["Unempfindlichkeit"].value;
        actorData.ini.value += actorData.skills["Reflexe"].value;
      }

      // Motivation/Roles/Profession vorbereiten
      // TODO: automatisieren (rules)
      actorData.motivation.available = true;
      actorData["role-1"].available = true;
      actorData["role-2"].available = this.level >= 2;
      actorData["role-3"].available = this.level >= 7;
      actorData.profession.available = this.level >= 2;

      // FIXME: items verlinken

      if (this.isHunter2e) {
        actorData.states = {};
        actorData.skills = {};
        actorData.combat = {};

        // recreate (shim) skills and combat structure from items
        const skillItems = (this.itemTypes.skill || []).sort((a, b) => a.name.localeCompare(b.name)); // returns items, not data
        for (let skill of skillItems) {
          if (skill.system.type === "general") {
            actorData.skills[skill.name] = {
              "id": skill.id,
              "label": skill.name ?? 'unnamed',
              "attribute": skill.system.attribute ?? 'SIN',
              "value": skill.system.value ?? 0
            };
          }
          else if (skill.system.type === "weapon") {
            actorData.combat[skill.name] = {
              "id": skill.id,
              "label": skill.name ?? 'unnamed',
              "attribute": skill.system.attribute ?? 'SIN',
              "value": skill.system.value ?? 0,
              "ap": skill.system.ap ?? 1,
              "schaden": skill.system.schaden ?? 0,
              "equipped": skill.system.equipped ?? true,
              "anmerkung": skill.system.anmerkung ?? ''
            };
          }
          else {
            console.error(`unknown skill subtype: ${skill.system.type}`);
          }
        }

        // resort weapon skills to logical order
        // FIXME Liste in Konfiguration auslagern
        const order = [
          'Fausthieb',
          'Dolche',
          'Fechtwaffen',
          'Schwerter',
          'Schlagwaffen',
          'Stangenwaffen',
          'Wurfwaffen',
          'Pistolen',
          'Armbrüste',
          'Musketen',
          'Ausweichen'
        ];
        let resorted = {}
        for (const skill of order) {
          if (skill in actorData.combat) {
            resorted[skill] = actorData.combat[skill];
            delete actorData.combat[skill];
          }
        }
        resorted = Object.assign(resorted, actorData.combat);
        actorData.combat = resorted;

        // recreate (shim) states from items
        const stateItems = (this.itemTypes.state || []).sort((a, b) => a.name.localeCompare(b.name)); // returns items, not data
        const stateMap = {'internal_damage': 'idmg', 'external_damage': 'odmg', 'penalty': 'mdmg', 'impediment': 'ldmg'};
        for (let state of stateItems) {
          actorData.states[state.name] = {
            "id": state.id,
            "type": stateMap[state.system.type] ?? state.system.type,
            "state": true,
            "effekt": state.system.effect,
            "abbau": state.system.removal,
            "hilfe": state.system.help === "" ? undefined : state.system.help,
            "special": state.system.special === "" ? undefined : state.system.special
          };
        }
      }
    }
    // nicht für npc-leader und npc-bande
    // else if ('npc' === this.type) {
    //   this._prepareNpcData();
    // }
  }

  // nicht für npc-leader und npc-bande
  _prepareNpcData() {
    const actor = this.data;
    const data = actor.data;
    this._recursivePrepareNumbers(data);
  }

  _recursivePrepareNumbers(data) {
    Object.keys(data).forEach(key => {
      const prop = data[key];
      if (!prop) return;
      if (prop.dtype) {
        if (prop.dtype === 'Number') {
          prop.value = prop.base;
        }
      }
      else if (prop.constructor.name === 'Object') {
        this._recursivePrepareNumbers(prop);
      }
      else if (Array.isArray(prop)) {
        prop.forEach(p => this._recursivePrepareNumbers(p));
      }
    });
  }

  // get motivation() {
  //   const m = this.data.items.filter(i => "motivation" === i.type);
  //   return m.length > 0 ? m[0] : undefined;
  // }

  // FIXME: überprüfen, Stand 0.7.x
  // Workflow for collection items:
  // ActorSheet._onDrop(): Fallunterscheidung Itemherkunft
  //   `--> Actor.importItemFromCollection(): lädt Entity aus der Compendium-Collection
  //          `--> callback_1
  //
  // (callback_1)
  //   `--> Actor.createOwnedItem(): Zuordung zu Entity-Collection "OwnedItems" des Actors
  //          `--> Actor.createEmbeddedEntity(): Auskopplung Token
  //                 `--> Entity.createEmbeddedEntity(): Entity "speichern" und verteilen
  //                        `--> callback_2
  //
  // (callback_2): je Client, selbst direkt, die anderen werden über Entity Socket Listener angetriggert
  //   `--> Entity.<static>_handleCreateEmbeddedEntity()
  //          `--> Actor.collection.push(): Fügt Rohdaten in Actor.data.<collection> ein
  //          `--> Actor._onCreateEmbeddedEntity(): je Entity
  //                 `--> Item.createOwned(): Erzeugt das eigentliche Item und fügt es zur Actor.items hinzu
  //          `--> Entity._onModifyEmbeddedEntity(): 1x pro Batch
  //                 `--> Actor.prepareData()


  // FIXME: mit createEmbeddedDocuments verschmelzen? Aufruf aus Jaeger-Sheet!
  async createOwnedItem(itemData, options = {}) {
    // TODO: Prüfung auf Custom-Markierung von data auf flags.hexxen-1733.compendium umstellen
    if (!itemData.data) {
      HexxenEntityHelper.addCustomFlag(itemData); // FIXME passt das mit dem custom flag noch? Wann wird create aufgerufen? Ist core.sourceID besser geeignet?
    }
    super.createEmbeddedDocuments('Item', [itemData], options);
  }

  // TODO: geht das auch über einen Hook? Bessere Codeposition?
  async createEmbeddedDocuments(embeddedName, documents, options={}) {
    documents = documents.reduce((ret, newItemData) => {
      if (this.isHunter) {

        // TODO: Berechtigungen prüfen
        if ("motivation" === newItemData.type) {
          const mot = this.items.filter( i => "motivation" === i.type );
          if (mot.length > 0) {
            ui.notifications.warn(`Es wurden bereits eine Motivation zugewiesen.`); // TODO: singular
            return;
          }
          // TODO: id eintragen
        }
        else if ("role" === newItemData.type) {
          // TODO: check if an additional role is allowed (Lv. 1/2/7)
          const level = this.level;
          const max = level >= 7 ? 3 : ( level >= 2 ? 2 : 1 );
          const roles = this.items.filter( i => "role" === i.type );
          if (roles.length >= max) {
            ui.notifications.warn(`Es wurden bereits ${max} Rollen zugewiesen.`); // TODO: singular
            return;
          }
          // TODO: check for duplicates überarbeiten (Kriterium?)
          else if (roles.filter( i => i.name === newItemData.name).length) {
            ui.notifications.warn(`Die Rolle ${newItemData.name} ist bereits zugewiesen.`);
            return;
          }
          // TODO: id eintragen
        }
        else if ("profession" === newItemData.type) {
          const prof = this.items.filter( i => "profession" === i.type );
          if (prof.length >= 1) {
            ui.notifications.warn(`Es wurde bereits eine Profession zugewiesen.`);
            return;
          }
          // TODO: Voraussetzungen prüfen
          // TODO: id eintragen
        }
        else if ("power" === newItemData.type) {
          const powers = this.items.filter( i => "power" === i.type );
          // TODO: check for duplicates überarbeiten (Kriterium?)
          if (powers.filter( i => i.name === newItemData.name).length) {
            ui.notifications.warn(`Die Jägerkraft ${newItemData.name} ist bereits zugewiesen.`);
            return;
          }
          // Ausbaukraft/Stammeffekt
          if ("ausbau" === newItemData.system.type) {
            // TODO: Ausbaukraft/Stammeffekt sicherstellen
            // via game.pack.getEntry die Daten holen
            // --> newItemData zu [] machen und beide übergeben
            // console.log(newItemData);
          }
          // TODO: Voraussetzungen prüfen
          // set sort value to a value higher than the highest used for this item type
          newItemData.sort = this._getHighestItemSortValue(powers) + 1000;
        }
        else if ("item" === newItemData.type) {
          const items = this.items.filter( i => "item" === i.type );
          // TODO: Voraussetzungen prüfen
          // set sort value to a value higher than the highest used for this item type
          newItemData.sort = this._getHighestItemSortValue(items) + 1000;
        }
        else if ("skill" === newItemData.type) {
          if (this.isHunter1e) {
            ui.notifications.warn(`Fertigkeiten können nur mit einem 2E Charakter verwendet werden.`);
            return;
          }
          const skills = this.items.filter( i => "skill" === i.type );
          // TODO: check for duplicates überarbeiten (Kriterium?)
          if (skills.filter( i => i.name === newItemData.name).length) {
            ui.notifications.warn(`Die Fertigkeit ${newItemData.name} ist bereits zugewiesen.`);
            return;
          }
        }
        else if ("state" === newItemData.type) {
          if (this.isHunter1e) {
            ui.notifications.warn(`Zustände können nur mit einem 2E Charakter verwendet werden.`);
            return;
          }
          const states = this.items.filter( i => "state" === i.type );
          // TODO: check for duplicates überarbeiten (Kriterium?)
          if (states.filter( i => i.name === newItemData.name).length) {
            ui.notifications.warn(`Der Zustand ${newItemData.name} ist bereits zugewiesen.`);
            return;
          }
        }
        else {
          // TODO: bereits im dropable blocken
          ui.notifications.warn(`Items vom Typ ${game.i18n.localize(`HEXXEN.ITEMTYPE.${newItemData.type}`)} können nicht zugewiesen werden!`);
          return;
        }
      } // end if character
      // else if ("npc" === this.type) {
      //   if ('npc-power' === newItemData.type) {
      //     // accept
      //   }
      //   else {
      //     // TODO: bereits im dropable blocken
      //     ui.notifications.warn(`Items vom Typ ${game.i18n.localize(`HEXXEN.ITEMTYPE.${newItemData.type}`)} können nicht zugewiesen werden!`);
      //     return;
      //   }
      // }
      else {
        // TODO: bereits im dropable blocken
        // FIXME: funktioniert nicht bei nicht verlinkten Tokens!! Auf Hook umstellen??
        // const allowed = Hooks.call("dropActorSheetData", actor, this, data);
        ui.notifications.warn(`Die Zuweisung von Items wird aktuell nicht unterstützt!`);
        return;
      }
      ret.push(newItemData);
      return ret;
    }, []);

    super.createEmbeddedDocuments(embeddedName, documents, options);
  }

  // TODO: delete --> id löschen
  _getHighestItemSortValue(items) {
    return items.reduce((ret, p) => Math.max(ret, p.sort), 0);
  }


  async update(formData) {
    // TODO Workaround für #8366
    Object.keys(formData).forEach(key => {
      if (key.startsWith('data.')) {
        const newKey = key.replace('data.', 'system.');
        formData[newKey] = formData[key];
        delete formData[key];
      }
    });

    return await super.update(formData);
  }


  /** @override */
  getRollData() {
    // FIXME: Workaround 0.8.x: duplicate verwenden, Änderungen an RollData ändern data!
    // FIXME: außer INI wird eigentlich nichts benötigt. super.getRollData() anschauen, evtl unnötig
    const data = duplicate(super.getRollData());

    // TODO: temporäre Modifikation, um SCs bei gleicher INI Vorrang vor NSCs zu geben.
    if (this.isHunter) {
      data.ini.value += 0.1;
    }

    // TODO: temporärer Konvertierungscode für INI bei NPCs, durch migration ersetzen
    if (this.isNpc) {
      data.ini = data.ini || {};
      data.ini.value = data.calc.ini;
    }

    return data;
  }

  // async _migrateData() {
  //   // console.info("Processing", this.isToken ? "Token" : "", this.data.name); // FIXME: entfernen

  //   // TODO: auf Loop (wie bei Items) umstellen

  //   // early actors did not have a "_data-revision" attribute (but is added through the template),
  //   // so check for "core" and "calc" which do not appear in later revisions
  //   const legacy = (this.data.data.core !== undefined) && (this.data.data.calc !== undefined);
  //   const revision = legacy ? 0 : this.data.data["_data-revision"];

  //   // TODO: vorgelagerte Prüfung (z.B. _needToMigrate())
  //   // TODO: handle tokens
  //   if (this.isToken) return;

  //   // to avoid multiple migration runs, migration is only done by gamemaster
  //   if (!game.user.isGM) return; //TODO: Hinweis für Spieler, dass Actor noch nicht migriert.

  //   if ("character" === this.type) {

  //     switch (revision) {
  //       case 0: {
  //         // FIXME: log entfernen
  //         console.info("migrate", this.data.name, duplicate(this.data));
  //         const updates = {}, remove = {};

  //         // FIXME: ifs verwenden
  //         updates["data.health.min"] = 0; // calculated
  //         updates["data.health.max"] = 0; // calculated
  //         updates["data.power.max"] = 0; // calculated

  //         updates["data.level.value"] = this.data.data.core.level;
  //         updates["data.languages.0.value"] = this.data.data.core.sprachen; // FIXME: array???
  //         updates["data.vitiation.value"] = this.data.data.core.verderbnis;

  //         updates["data.temp.pw"] = this.data.data.calc.pw;

  //         updates["data.notes.biography.editor"] = this.data.data["biography"];
  //         updates["data.notes.states.editor"] = this.data.data["states-text"];
  //         updates["data.notes.skills.editor"] = this.data.data["skills-text"];
  //         updates["data.notes.powers.editor"] = this.data.data["powers-text"];
  //         updates["data.notes.combat.editor"] = this.data.data["combat-text"];
  //         updates["data.notes.items.editor"] = this.data.data["items-text"];

  //         // // identify motivation/role/profession itemId
  //         const mot = this.data.items.filter(i => "motivation" === i.type);
  //         if (mot.length > 0) {
  //           updates["data.motivation.itemId"] = mot[0].id;
  //         }
  //         const role = this.data.items.filter(i => "role" === i.type);
  //         for (let i = 0; i < 3; i++) {
  //           if (role.length > i) {
  //             updates[`data.role-${i+1}.itemId`] = role[i].id;
  //           }
  //         }
  //         const prof = this.data.items.filter(i => "profession" === i.type);
  //         if (prof.length > 0) {
  //           updates["data.profession.itemId"] = prof[0].id;
  //         }

  //         remove["data"] = {
  //           "-=core": null,
  //           "-=calc": null,
  //           "-=biography": null,
  //           "-=states-text": null,
  //           "-=skills-text": null,
  //           "-=powers-text": null,
  //           "-=combat-text": null,
  //           "-=items-text": null
  //         };
  //         remove["data.power"] = {
  //           "-=min": null
  //         };

  //         // FIXME: erster update (setFlag ruft update auf) schlägt fehl, da game.actors nicht definiert ist.
  //         console.log(this.name, "update 1");
  //         await this.setFlag(Hexxen.scope, "data-revision", 1); // update will fail (error: collection not defined)
  //         console.log(this.name, "update 2");
  //         await this.update(updates);
  //         console.log(this.name, "update 3");
  //         await this.update(remove);

  //         // FIXME: log entfernen
  //         console.info("after migrate", this.data.name, duplicate(this.data));
  //         // continue
  //       }
  //       case 1:
  //       default:
  //     }
  //   }

  // }

}
